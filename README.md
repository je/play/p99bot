<h1><div align="center">Project 1999 Bot</div></h1>

[TOC]

# Overview

P99Bot is a set of Docker containers and system infrastructure
that coordinates to implement a system whereby a user can run
Project 1999 in a semi-unattended way[^1] and send messages (i.e.
game logs) to a Redis server for processing and distribution.

This mode of operation can be useful for bots--characters that
just sit in game and relay information out of game to be used by
other tools. For example: monitoring guild chat for profanity or
racist language.

This system consists of three Docker containers, each of which
builds on top of the previous one for additional functionality:

1. [**p99bot-os-amd64**](https://gitlab.com/je/play/p99bot-os): The base Ubuntu Linux operating system
	container. This container provides underlying OS and management
	infrastructure for the other layers.

2. [**p99bot-p99-amd64**](https://gitlab.com/je/play/p99bot-p99): Adds the Project 1999 EverQuest client
	and the xpra tool to the OS layer. xpra is a tool that allows
	accessing an X11 session over the web with just a browser.

3. [**p99bot-relay-amd64**](https://gitlab.com/je/play/p99bot-relay): Adds capability to relay lines from
	game log files to a Redis server via pub-sub.

Each of these layers is an independent, usable container system.
For example, if one simply wants to play Project 1999, one only
needs to create a container using the "p99" image, and ignoring
the additional complexity of the relay layer. Use cases other
than log relay could be implemented using the same "p99" image.

# Creation

## Building the Containers

Typically, to build the containers, one can run a command from
the repository root like:

    docker build -f src/app/Dockerfile -t "p99bot-foo-amd64:latest" -t "p99bot-foo-amd64:1" --progress plain .

To save the container to a portable image file, one can run:

    docker save "p99bot-foo-amd64:1" | xz -8vcT 0 > "p99bot-foo-amd64:1.xz"

To simultaneously save the container to a compressed image file,
upload the image to a VM, and load it into Docker on the VM:

    docker save p99bot-foo-amd64:1 \
        | xz -8vcT 0 - \
        | tee "p99bot-foo-amd64:1.xz" \
        | ssh user@host "xzcat -c - | sudo docker load"

One could also distribute the container via a registry such as
Docker Hub.

# Operation

## Preparation

The host running the container will need Docker installed (or
another container engine) and the container image loaded into
said container engine. E.g., for an Ubuntu host and Docker:

    sudo snap install docker \
      && xzcat "p99bot-foo-amd64:1.xz" \
      | sudo docker image load

Using something like Docker Hub would also work, of course.

### AWS EC2 Instance Types

A lot of testing was done with different types of EC2 instances
to find the cheapest serviceable instance for different use cases.
This is of course somewhat subjective but these are the results:

#### Use Case: Bots

For a simple, stationary, unattended bot, the best instance type
at this time is t3.micro, or t3.nano with 2+ GB of swap space.
This reliably runs the game and can be played manually if needed,
though it is slow and difficult to operate (really bad FPS and
high-latency controls). Instance types smaller than this had
frequent client crashes, loaded so slowly they got disconnected,
or had other similar reliability issues.

Note that with the level of resources this instance type provides,
the instance typically runs at 100%+ CPU and memory. *All the time.*
This is because xpra is emulating the 3D operations; there is no
GPU (instance types with GPUs are significantly more expensive),
so the CPU is doing both the normal CPU work and the GPU work.

Similar instance types that were tested include t3a.micro, t3.small
and t3a.small. The t3a.micro instance type is slightly cheaper
than t3.micro but the CPU has a lower clock speed which has a
large effect on this type of workload. The other two options
offered similar performance but were more expensive.

Note that for small instance types with less than 2 GB RAM, one
is advised to add some swap to the host system (i.e., not the
container).

#### Use Case: Gaming

For actually playing the game manually, which requires relatively
responsive controls and graphics, and considering that there is
no GPU, one needs significantly more CPU resources. The game is
playable, though just barely, using any of these instance types:
c6a.large, t3a.xlarge, t3.xlarge. These instance types struggle
to be usable in areas of high population (such as the East Commons
tunnel) or activity (raiding). For solo play, however, they might
work.

Of these options, the cheapest option is c6a.large, which also has
the fewest CPU and RAM resources. However, this instance type is
optimized for CPU, which is what this method of operation (running
via emulated 3D graphics) really needs. Currently, c6a.large costs
about half as much as the other options. It should be noted that
the next tier up, c6a.xlarge, is usable everywhere without issue.

Sadly, the cheapest options, Gravitron or ARM CPU instance types,
won't work. Even though Linux, WINE and xpra support ARM CPUs,
there is no ARM-compatible version of EverQuest (and likely will
never be).

It should be noted that the Paperspace Air VM runs Project 1999
fairly well also, as it includes an actual GPU with the VM. The
interfaces, both web and desktop clients, provide are a much
better experience over xpra and AWS. However, these VMs are the
same price as a c6a.large on-demand instance. One can buy a
reserved instance from AWS to drop that price, but Paperspace
currently has no similar reservation discount.

### Image Management

The easiest way to manage the containers (without a registry)
seems to be via a shared volume. This allows transferring the
image over a slow link (uploading) only once, and using the
faster datacenter network to transfer from the volume to VMs.

Initially one will need to create a new volume, attach and mount
it in a VM, transfer image(s) to it, and unmount and detach it.
It is recommended to apply a filesystem label to the mount before
unmounting it, so in the future one doesn't need to hunt for the
physical drive name, e.g.:

    sudo mount /dev/disk/by-label/p99-images /mnt

If the volume label doesn't exist, `/dev/nvme1n1` may work, but
the correct drive path may vary based on several factors.

## Usage

Typical startup will execute the `/p99bot/init` bash script. This
script executes `run-parts` on the `/p99bot/rc.d` directory,
executing any scripts within in lexical order. This container
includes no scripts within this folder, only the structure to
execute them.

If the user executes the container with the `-t -i` options, the
script will prompt the user to choose between exiting or entering
a bash shell. The script will then wait for a response. Using
either `-t` or `-i` individually may prompt, but won't work (the
input isn't detected).

---

[^1]: No claim is made that running Project 1999 in this manner
    is permitted by server rules or administrators --use at your
    own risk!
